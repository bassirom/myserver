# Myserver

## Introduction

Ce projet permet de déployer un serveur Linux. On y trouve des instructions, des fichiers de configuration.

## Instructions 

Clonez le projet dans votre dossier personnel:

```

git clone https://gitlab.com/bassiro/myserver.git

```

Entrez maintenant dans le dossier téléchargé:
    cd myserver

Vous pouvez maintenant:
- installer un serveur web
- intsaller un serveur ssh


### Installer un serveur web Nginx

#### Quelques éléments sur le réseau

Pour connaitre son adresse ip:
```
ip a
```

On cherche la ligne qui contient inet (`ip a |grep inet`)

Ceci est l'adresse de notre ordinateur sur le réseau interne.

A ne pas confondre avec votre adresse ip publique, qui est l'adresse de votre BOX, accesible depuis Internet.

Pour obtenir cette dernière, tapez "what is my ip ?" dans la barre de recherche Google.

##### Connaitre ses nom d'hote

On utilise le fichier `/etc/hosts`, qui fonctionne comme un serveur DNS: il s'agit d'un tableau de correspondance entre des adresses IP et des noms d'hote.

On va ainsi pouvoir communiquer avec l'ordinateur en utilisant au choix l'adresse IP ou le nom d'hote.

Ex:
```
127.0.0.1       localhost
127.0.1.1       bassiro.garage.gn       bassiro

##### Installer nginx

```
sudo apt install nginx
```

Pour vérifier que le serveur fonctionne, on utilise:
```

sudo systemctl status nginx
```
Si le status de nginx est "Inactive", on le démarre avec 
```
sudo systemctl start nginx
```

##### Modifier le contenu

La page par défaut d'accès a nginx se trouve à `/var/www/html/index.nginx-debian.html`

Modifiez ce fichier pour changer la page d'accueil.

```
sudo nano /var/www/htmal/index.nginx-debian.html
```
